variable "region" {
  default     = "us-east-1"
  description = "AWS region"
}

variable "cluster_name" {
  default     = "vote-result-app"
  description = "EKS Cluster name"
}

variable "cluster_version" {
  default     = "1.22"
  description = "Kubernetes version"
}

variable "instance_type" {
  default     = "t2.micro"
  description = "EKS node instance type"
}

variable "instance_count" {
  default     = 3
  description = "EKS node count"
}

variable "agent_version" {
  default     = "v14.8.1"
  description = "Agent version"
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "agent_token" {
  description = "Agent token (provided after registering an Agent in GitLab)"
  sensitive   = true
}

variable "kas_address" {
  description = "Agent Server address (provided after registering an Agent in GitLab)"
}

variable "profile" {
  default     = "default"
  description = "Kubernetes namespace to install the Agent"
}

variable "cw_retention_in_days" {
  default = 3
}