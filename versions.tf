terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.10.0"
    }
    local = {
      version = ">= 1.4.0"
    }
    null = {
      version = ">= 2.1.2"
    }
    template = {
      version = ">= 2.1.2"
    }
  }
}
