resource "null_resource" "install_kube_manifests" {
  depends_on = [module.eks]

  provisioner "local-exec" {
    working_dir = path.module

    interpreter = ["/bin/sh", "-c"]

    command = <<EOS
mkdir -p temp;
echo '${module.eks.kubeconfig}' > temp/kubeconfig
echo '${null_resource.install_kube_manifests.triggers.db-deployment_rendered}' > temp/db-deployment.yaml;
echo '${null_resource.install_kube_manifests.triggers.db-service_rendered}' > temp/db-service.yaml;
echo '${null_resource.install_kube_manifests.triggers.redis-deployment_rendered}' > temp/redis-deployment.yaml;
echo '${null_resource.install_kube_manifests.triggers.result-deployment_rendered}' > temp/result-deployment.yaml;
echo '${null_resource.install_kube_manifests.triggers.vote-deployment_rendered}' > temp/vote-deployment.yaml;
echo '${null_resource.install_kube_manifests.triggers.vote-namespace_rendered}' > temp/vote-namespace.yaml;
echo '${null_resource.install_kube_manifests.triggers.worker-deployment_rendered}' > temp/worker-deployment.yaml;
echo '${null_resource.install_kube_manifests.triggers.redis-service_rendered}' > temp/redis-service.yaml;
echo '${null_resource.install_kube_manifests.triggers.result-service_rendered}' > temp/result-service.yaml;
echo '${null_resource.install_kube_manifests.triggers.vote-service_rendered}' > temp/vote-service.yaml;
kubectl apply -f temp/ --kubeconfig temp/kubeconfig
rm -r temp;
EOS

  }

  triggers = {
    db-deployment_rendered     = data.template_file.db-deployment.rendered
    db-service_rendered        = data.template_file.db-service.rendered
    redis-deployment_rendered  = data.template_file.redis-deployment.rendered
    result-deployment_rendered = data.template_file.result-deployment.rendered
    vote-deployment_rendered   = data.template_file.vote-deployment.rendered
    vote-namespace_rendered    = data.template_file.vote-namespace.rendered
    worker-deployment_rendered = data.template_file.worker-deployment.rendered
    redis-service_rendered     = data.template_file.redis-service.rendered
    result-service_rendered    = data.template_file.result-service.rendered
    vote-service_rendered      = data.template_file.vote-service.rendered
  }

}

# resource "null_resource" "get_vote_external_ip" {
#   triggers  =  { always_run = "${timestamp()}" }
#   provisioner "local-exec" {
#     command = "kubectl get services --namespace ${ingress-nginx} vote --output jsonpath='{.status.loadBalancer.ingress[0].ip}' >> ${path.module}/vote_external_ip.txt"
#   }
#   depends_on = ["null_resource.install_kube_manifests"]
# }

# data "local_file" "prometheus-ip" {
#     filename = "${path.module}/prometheus_private_ips.txt"
#   depends_on = ["null_resource.get_vote_external_ip"]
# }