module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.0.0"

  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version

  subnets = module.vpc.private_subnets

  node_groups_defaults = {
    default = {
      min_size       = 1
      max_size       = var.instance_count
      desired_size   = var.instance_count
      instance_types = [var.instance_type]
    }
  }
  vpc_id = module.vpc.vpc_id

  kubeconfig_aws_authenticator_command         = "aws"
  kubeconfig_aws_authenticator_command_args    = ["eks", "update-kubeconfig"]
  kubeconfig_aws_authenticator_additional_args = ["--name", var.cluster_name, "--alias", var.cluster_name]
  kubeconfig_aws_authenticator_env_variables = {
    AWS_PROFILE        = var.profile
    AWS_DEFAULT_REGION = var.region
  }

}
