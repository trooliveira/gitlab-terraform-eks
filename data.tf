data "aws_availability_zones" "available" {}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "template_file" "db-deployment" {
  template = file("${path.module}/templates/db-deployment.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "db-service" {
  template = file("${path.module}/templates/db-service.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "redis-deployment" {
  template = file("${path.module}/templates/redis-deployment.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "result-deployment" {
  template = file("${path.module}/templates/result-deployment.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "redis-service" {
  template = file("${path.module}/templates/redis-service.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "result-service" {
  template = file("${path.module}/templates/result-service.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "vote-deployment" {
  template = file("${path.module}/templates/vote-deployment.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "vote-namespace" {
  template = file("${path.module}/templates/vote-namespace.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "vote-service" {
  template = file("${path.module}/templates/vote-service.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}
data "template_file" "worker-deployment" {
  template = file("${path.module}/templates/worker-deployment.yaml.tpl")

  vars = {
    cluster_name         = var.cluster_name
    region               = var.region
    cw_retention_in_days = var.cw_retention_in_days
  }
}